# Template for Gitlab hosted JUCE projects (WIP) 

[![pipeline status](https://gitlab.com/2-shell/template-juce-full/badges/main/pipeline.svg)](https://gitlab.com/2-shell/template-juce-full/-/commits/main)
[![coverage report](https://gitlab.com/2-shell/template-juce-full/badges/main/coverage.svg)](https://gitlab.com/2-shell/template-juce-full/-/commits/main)
[![Latest Release](https://gitlab.com/2-shell/template-juce-full/-/badges/release.svg)](https://gitlab.com/2-shell/template-juce-full/-/releases) 
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![cookiecutter](https://img.shields.io/badge/cookiecutter-2.6.0-blue?logo=cookiecutter)](https://github.com/audreyr/cookiecutter)
[![C++ 20](https://img.shields.io/badge/C++-20-blue?logo=cplusplus)](https://en.cppreference.com/w/cpp/20)
[![Windows](https://img.shields.io/badge/Windows-10-0078D6?logo=windows)](https://www.microsoft.com)
[![macOS](https://img.shields.io/badge/macOS-10.13-blue?logo=apple)](https://www.apple.com)
[![Linux](https://img.shields.io/badge/Linux-amd64-blue?logo=linux)](https://www.linux.org)
[![CMake](https://img.shields.io/badge/CMake-3.25-064F8C?logo=cmake)](https://cmake.org)
[![GitVersion](https://img.shields.io/badge/GitVersion-5.12.0-blue)](https://gitversion.net/docs/)
[![JUCE](https://img.shields.io/badge/JUCE-8.0.0-6DA61F?logo=juce)](https://juce.com)
[![Catch2](https://img.shields.io/badge/Catch2-2.13.7-68217A?logo=catch2)](https://github.com/catchorg/Catch2)
[![Gitlab CI](https://img.shields.io/badge/Gitlab-CI-FC6D26?logo=gitlab)](https://gitlab.com)
[![LLVM Cov](https://img.shields.io/badge/LLVM-Cov-blue?logo=llvm)](https://llvm.org/docs/CommandGuide/llvm-cov.html)
[![Clang Tidy](https://img.shields.io/badge/Clang-tidy-blue?logo=clang)](https://clang.llvm.org/extra/clang-tidy/)
[![Clang Format](https://img.shields.io/badge/Clang-format-blue?logo=clang)](https://clang.llvm.org/docs/ClangFormat.html)
[![Pluginval](https://img.shields.io/badge/Pluginval-1.0.3-blue)](https://github.com/Tracktion/pluginval)

This is a [cookiecutter](https://github.com/audreyr/cookiecutter) template for JUCE projects with extended integration
with Gitlab and Gitlab CI.  
It's intended to serve as a elaborate starting point for quickly getting started with JUCE development.

## Features

- Cross-Platform JUCE template covering Linux, Windows and macOS
  - pre-populated source tree layout
  - example / stub code covering all major JUCE project types: CLI, GUI App and Audio Plugin
  - additionally, shared code and binary resources
  - JUCE plugin set up to build Standalone, VST3, AU, LV2 and Unity Plugins
- Infrastructure for unit tests and TDD based on Catch2
  - Dedicated testrunner project
  - Integration of running tests through CTest
  - Example tests covering shared code
  - Creates junit reports for CI
- Hierarchical CMake project structure with modern CMake features
  - CMake 3.25 or newer required
  - Automated retrieval of source code dependencies (via CMake FetchContent)
    - JUCE
    - melatonin inspector _(optional)_
    - catch2
    - trompeloeil _(optional)_
    - httplib _(optional)_
  - CCache integration for all platforms
  - clang-tidy integration
  - clang-format (linting) integration
  - LLVM Code Coverage integration
  - GitVersion integration
  - Pluginval integration
  - Various CMake functions and macros for easier project setup
  - Most tasks performed in CI are also available locally
- Gitlab CI configuration
  - Build jobs for all platforms with artifact storage
  - Docker-based jobs for Linux, facilitating custom docker image builds
  - Test jobs including coverage calculation and reporting test results and coverage
  - Code quality jobs, including clang-tidy and clang-format checks with reporting in various formats
  - Automated versioning based on GitVersion
  - Plugin validation jobs for all plugin types (supported by Pluginval) on all platforms
- Extended Gitlab UI integration
  - Unit test trend, overall results and details in MergeRequests
  - Unit test results and details in Pipeline / Job view
  - Test coverage trend and result in MergeRequests and Pipeline / Job view
  - Test coverage visualization in MergeRequests diff viewer
  - Code quality trend or results in MergeRequests
  - Code quality details in Pipeline / Job view
  - Badges for pipeline status, coverage and latest release
- .clang-format template
- .clang-tidy template
- .editorconfig template
- .gitignore template, covering all supported platforms, toolchains and most relevant IDEs
- .gitattributes template, covering preconfigured Git-LFS for binary resources
- .git-blame-ignore-revs template _(for ignoring specifig commits in git blame)_
- Minimal GitVersion configuration _(to allow adjusting to project needs)_
- MIT licensed, allowing for easy integration into commercial projects

## Pre-requisites

You'll need to have [cookiecutter installed](https://github.com/audreyr/cookiecutter).
This is a Python tool - on most platforms, install using:

```
$ pip3 install cookiecutter
```

## Generating the project

Start off by invoking [cookiecutter](https://github.com/audreyr/cookiecutter).
This can be done locally:

```
python -m cookiecutter <path-to-this-template>
```

or directly from the GitLab URL where the template lives:

```
python -m cookiecutter https://gitlab.com/2-shell/template-juce-full.git
```

When the template is generated, cookiecutter will interactively prompt for a number of parameter values and options
which configure general settings and controls optional features.

Here's an example dialog:
```
  [1/21] Full, informal name of the project (My new JUCE project): Awesome JUCE Project
  [2/21] Short project description (Template for JUCE CMake projects - covers all JUCE target types): This plugin revolutionalizes audio processing as we know it!
  [3/21] Project homepage URL (https://gitlab.com/2-shell/template-juce-full): http://awesome-plugin.com
  [4/21] Have CMake fetch Melatonin Inspector? [y/n] (y):
  [5/21] cmake_fetch_trompeloeil [y/n] (y):
  [6/21] Have CMake fetch httplib? [y/n] (n): n      
  [7/21] Root namespace (CMake and C++) (ajp): awesome
  [8/21] Add CLI target? [y/n] (y):
  [9/21] CLI app name (Example Console App): awesome-cli
  [10/21] Use JUCEHeader for CLI App? [y/n] (n):
  [11/21] Add Data target to CLI App? [y/n] (n):
  [12/21] Add GUI App target? [y/n] (y):
  [13/21] GUI app name (Example GUI App):  
  [14/21] Use JUCEHeader for GUI App? [y/n] (n):
  [15/21] Add Data target to GUI App? [y/n] (n): y
  [16/21] Add Plugin target? [y/n] (y):
  [17/21] Plugin name (Example Audio Plugin): Awesome Plugin
  [18/21] Plugin manufacturer code (4 characters, letters or numbers, at least one uppercase) (Awe0):  
  [19/21] Unique plugin code (4 characters, letters or numbers, at least one uppercase) (Ap00): Awp1
  [20/21] Use JUCEHeader for Plugin? [y/n] (n):
  [21/21] Add Data target to Plugin? [y/n] (n):
```

For details on available template parameters, see below.

The newly generated project will be output to a subdirectory of the working directory using a slugified version of the
project name.  
It has its own README with further instructions on how to build and run the project.

## Template parameters

### Public parameters

| Parameter                         | Description                                                                                                                   |
|-----------------------------------|-------------------------------------------------------------------------------------------------------------------------------|
| `project_name`                    | Full, informal name of the project. Used to derive directory, CMake project and namespace names as well as variable prefixes. |
| `project_description`             | Short project description. Used in README and CMake project.                                                                  |
| `project_homepage_url`            | URL to the project homepage. Used in README and CMake project.                                                                |
| `cmake_fetch_melatonin_inspector` | Add [Melatonin Component Inspector](https://github.com/sudara/melatonin_inspector) to the GUI App and Plugin targets.         | 
| `cmake_fetch_trompeloeil`         | Install [Trompeloeil](https://github.com/rollbear/trompeloeil) as a dependency for the testrunner target.                     |
| `cmake_fetch_httplib`             | Install [httplib](https://github.com/yhirose/cpp-httplib) as a dependency for the testrunner target.                          |
| `root_namespace`                  | Root namespace for CMake and C++ code. Default is derived from the project name (initials).                                   |
| `target_cli`                      | Add the CLI target to the project.                                                                                            |
| `target_cli_name`                 | Name of the CLI App.                                                                                                          |
| `target_cli_add_juceheader`       | Generate a JuceHeader for the CLI App target.                                                                                 |
| `target_cli_add_data_target`      | Add a target for compiling binary data to the CLI App target.                                                                 |
| `target_gui`                      | Add the GUI App target to the project.                                                                                        |
| `target_gui_name`                 | Name of the GUI App.                                                                                                          |
| `target_gui_add_juceheader`       | Generate a JuceHeader for the GUI App target.                                                                                 |
| `target_gui_add_data_target`      | Add a target for compiling binary data to the GUI App target.                                                                 |
| `target_plugin`                   | Add the Plugin target to the project.                                                                                         |
| `target_plugin_name`              | Name of the Plugin.                                                                                                           |
| `target_plugin_manufacturer_code` | Manufacturer code for the Plugin target. Autogenerated from project name by default.                                          |
| `target_plugin_plugin_code`       | Unique code for the Plugin target. Autogenerated from the target name by default.                                             |
| `target_plugin_add_juceheader`    | Generate a JuceHeader for the Plugin target.                                                                                  |
| `target_plugin_add_data_target`   | Add a target for compiling binary data to the Plugin target.                                                                  |

### Internal / Derived parameters

| Parameter                                | Description                                                                                  |
|------------------------------------------|----------------------------------------------------------------------------------------------|
| `__project_slug`                         | Slugified version of the project name. Used as the directory name for the generated project. |
| `__cmake_project_idenfitier`             | Identifier for the CMake project. Camelcase project name with spaces removed.                |
| `__cmake_var_prefix`                     | Prefix for CMake and environment variables. Uppercase initials of the project name.          |
| `__target_cli_cmake_idenfitier`          | Identifier for the CLI target. Camelcase CLI target name with spaces removed.                |
| `__target_cli_namespace`                 | Namespace for the CLI target. Lowercase CLI target CMake identifier.                         |
| `__target_gui_cmake_idenfitier`          | Identifier for the GUI target. Camelcase GUI target name with spaces removed.                |
| `__target_gui_namespace`                 | Namespace for the GUI target. Lowercase GUI target CMake identifier.                         |
| `__target_plugin_cmake_idenfitier`       | Identifier for the Plugin target. Camelcase Plugin target name with spaces removed.          |
| `__target_plugin_namespace`              | Namespace for the Plugin target. Lowercase Plugin target CMake identifier.                   |
| `_project_template_gitlab_ci_test_build` | Flag to indicate a nested build in the template's Gitlab CI configuration.                   |

## Template development

Template development happens in the `{{ cookiecutter.__project_slug }}` directory.  
Obviously, it's not possible to run builds from this directory directly, so it has to be instantiated to a temporary
location first.
 
To avoid having to manually configuring template parameters every time, run the following command from the root of the
template repository:

```
python -m cookiecutter --no-input --config-file test/test-config.yaml -f .
```

This will create a new project in the current directory in `template-juce-full`.  
The command above can be re-run at any time to update the generated project with changes made to the template.

For streamlined development in CLion, the project in this repository is set up to automatically run the above command
when files in the template directory change and build from the generated project directory.
