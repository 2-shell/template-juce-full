#include "Dummy.h"
#include "TestData.h"
#include <catch2/catch_test_macros.hpp>
{% if cookiecutter.cmake_fetch_httplib %}
#include <thread>
#include <httplib.h>
{% endif %}
{% if cookiecutter.cmake_fetch_trompeloeil %}
#include <catch2/trompeloeil.hpp>

template<typename ExitFunction>
class scope_exit
{
  public:
    explicit scope_exit (ExitFunction&& f) : exitFunction_ (std::forward<ExitFunction> (f)) {}
    ~scope_exit () { exitFunction_ (); }

  private:
    ExitFunction exitFunction_;
};

class MockNameRegistry : public {{ cookiecutter.root_namespace }}::shared::NameRegistry
{
  public:
    MAKE_CONST_MOCK1 (isNameRegistered, bool (const std::string&), override);
};
{% endif %}

TEST_CASE ("Run tests on dummy", "[Dummy]")
{
    SECTION ("Test constructor")
    {
        {{ cookiecutter.root_namespace }}::shared::Dummy dummy {"default"};
        REQUIRE (dummy.getName () == "default");
    }

    SECTION ("Test getter and setter")
    {
        {{ cookiecutter.root_namespace }}::shared::Dummy dummy{"default"}; // formatting intentionally wrong
        dummy.setName("42"); // formatting intentionally wrong
        REQUIRE (dummy.getName () == "42");
    }

    SECTION ("Test using TestData")
    {
        {{ cookiecutter.root_namespace }}::shared::Dummy dummy {TestData::test_data_txt};
        REQUIRE (dummy.getName () == TestData::test_data_txt);
    }

    SECTION ("Test bar")
    {
        {{ cookiecutter.root_namespace }}::shared::Dummy dummy1 {""};
        dummy1.bar ();
        REQUIRE (dummy1.getName () == "bar");

        {{ cookiecutter.root_namespace }}::shared::Dummy dummy2 {"foo"};
        dummy2.bar ();
        REQUIRE (dummy2.getName () == "foobar");
    }

    {% if cookiecutter.cmake_fetch_trompeloeil %}
    SECTION ("Test isNameAllowed")
    {
        MockNameRegistry registry;
        REQUIRE_CALL (registry, isNameRegistered ("foo")).RETURN (true);
        REQUIRE_CALL (registry, isNameRegistered ("bar")).RETURN (false);

        REQUIRE ({{ cookiecutter.root_namespace }}::shared::Dummy::isNameAllowed (registry, "foo"));
        REQUIRE_FALSE ({{ cookiecutter.root_namespace }}::shared::Dummy::isNameAllowed (registry, "bar"));
    }
    {% endif %}
}

{% if cookiecutter.cmake_fetch_httplib %}
TEST_CASE ("Run test using httplib", "[httplib]")
{
    httplib::Server srv;
    srv.Get ("/", [] (const httplib::Request&, httplib::Response& res) {
        res.set_content ("Hello World!", "text/plain");
    });
    std::thread server_thread ([&] { srv.listen ("localhost", 1234); });
    srv.wait_until_ready ();

    scope_exit stop_server ([&] {
        srv.stop ();
        server_thread.join ();
        REQUIRE (!srv.is_running ());
    });

    httplib::Client cli ("localhost", 1234);
    httplib::Result res;
    REQUIRE_NOTHROW (res = cli.Get ("/"));
    REQUIRE (res->status == 200);
    REQUIRE (res->body == "Hello World!");
}
{% endif %}
