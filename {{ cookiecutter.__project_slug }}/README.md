# {{ cookiecutter.project_name }}

{{ cookiecutter.project_description }}

[![pipeline status](https://gitlab.com/2-shell/template-juce-full/badges/main/pipeline.svg)](https://gitlab.com/2-shell/template-juce-full/-/commits/main)
[![coverage report](https://gitlab.com/2-shell/template-juce-full/badges/main/coverage.svg)](https://gitlab.com/2-shell/template-juce-full/-/commits/main)
[![Latest Release](https://gitlab.com/2-shell/template-juce-full/-/badges/release.svg)](https://gitlab.com/2-shell/template-juce-full/-/releases) 
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![C++ 20](https://img.shields.io/badge/C++-20-blue?logo=cplusplus)](https://en.cppreference.com/w/cpp/20)
[![Windows](https://img.shields.io/badge/Windows-10-0078D6?logo=windows)](https://www.microsoft.com)
[![macOS](https://img.shields.io/badge/macOS-10.13-blue?logo=apple)](https://www.apple.com)
[![Linux](https://img.shields.io/badge/Linux-amd64-blue?logo=linux)](https://www.linux.org)
[![CMake](https://img.shields.io/badge/CMake-3.25-064F8C?logo=cmake)](https://cmake.org)
[![GitVersion](https://img.shields.io/badge/GitVersion-5.12.0-blue)](https://gitversion.net/docs/)
[![JUCE](https://img.shields.io/badge/JUCE-8.0.0-6DA61F?logo=juce)](https://juce.com)
[![Catch2](https://img.shields.io/badge/Catch2-2.13.7-68217A?logo=catch2)](https://github.com/catchorg/Catch2)
[![Gitlab CI](https://img.shields.io/badge/Gitlab-CI-FC6D26?logo=gitlab)](https://gitlab.com)
[![LLVM Cov](https://img.shields.io/badge/LLVM-Cov-blue?logo=llvm)](https://llvm.org/docs/CommandGuide/llvm-cov.html)
[![Clang Tidy](https://img.shields.io/badge/Clang-tidy-blue?logo=clang)](https://clang.llvm.org/extra/clang-tidy/)
[![Clang Format](https://img.shields.io/badge/Clang-format-blue?logo=clang)](https://clang.llvm.org/docs/ClangFormat.html)
[![Pluginval](https://img.shields.io/badge/Pluginval-1.0.3-blue)](https://github.com/Tracktion/pluginval)

## Features

- Cross-Platform JUCE template covering Linux, Windows and macOS
  - pre-populated source tree layout
  - example / stub code covering all major JUCE project types: CLI, GUI App and Audio Plugin
  - additionally, shared code and binary resources
  - JUCE plugin set up to build Standalone, VST3, AU, LV2 and Unity Plugins
- Infrastructure for unit tests and TDD based on Catch2
  - Dedicated testrunner project
  - Integration of running tests through CTest
  - Example tests covering shared code
  - Creates junit reports for CI
- Hierarchical CMake project structure with modern CMake features
  - CMake 3.25 or newer required
  - Automated retrieval of source code dependencies (via CMake FetchContent)
    - JUCE
    - catch2
    - trompeloeil _(optional)_
    - httplib _(optional)_
  - CCache integration for all platforms
  - clang-tidy integration
  - clang-format (linting) integration
  - LLVM Code Coverage integration
  - GitVersion integration
  - Pluginval integration
  - Various CMake functions and macros for easier project setup
  - Most tasks performed in CI are also available locally
- Gitlab CI configuration
  - Build jobs for all platforms with artifact storage
  - Docker-based jobs for Linux, facilitating custom docker image builds
  - Test jobs including coverage calculation and reporting test results and coverage
  - Code quality jobs, including clang-tidy and clang-format checks with reporting in various formats
  - Automated versioning based on GitVersion
  - Plugin validation jobs for all plugin types (supported by Pluginval) on all platforms
- Extended Gitlab UI integration
  - Unit test trend, overall results and details in MergeRequests
  - Unit test results and details in Pipeline / Job view
  - Test coverage trend and result in MergeRequests and Pipeline / Job view
  - Test coverage visualization in MergeRequests diff viewer
  - Code quality trend or results in MergeRequests
  - Code quality details in Pipeline / Job view
  - Badges for pipeline status, coverage and latest release
- .clang-format template
- .clang-tidy template
- .editorconfig template
- .gitignore template, covering all supported platforms, toolchains and most relevant IDEs
- .gitattributes template, covering preconfigured Git-LFS for binary resources
- .git-blame-ignore-revs template _(for ignoring specifig commits in git blame)_
- Minimal GitVersion configuration _(to allow adjusting to project needs)_
- MIT licensed, allowing for easy integration into commercial projects

## CMake modules and scripts

See [doc/cmake.md](doc/cmake.md) for a detailed description of the CMake modules and scripts provided with this
template.
