# Juce Template - CMake modules and scripts

This repository contains a collection of CMake modules and scripts that are useful for JUCE projects, or C++ projects in
general.  
The modules are designed to be used with a modern CMake version (3.19 or newer) and are intended to be included in a
JUCE project's CMakeLists.txt file.

## `CCache.cmake`

The [`CCache`](../cmake/CCache.cmake) module enables using the [`ccache` compiler cache](https://ccache.dev/) with a
CMake project, if available.  
`ccache` can significantly speed up the build process by caching compilation results and reusing them when possible.
  
Generally, all that's needed to enable `ccache` is to include this module in the project's top-level `CMakeLists.txt`
file, making sure to include it after the `project()` and any `enable_language()` calls.

The module will detect whether `ccache` is available and uses CMake's `CMAKE_<LANG>_COMPILER_LAUNCHER` variables to set
the `ccache` command as the compiler launcher for each supported language.

Also, a function `ccache_fix_target_compile_options()` is provided, which can be used to adjust compile options of a
target that might interfere with `ccache` operation.

This approach works for most generators, but there are some exceptions. Specifically, the Xcode and Visual Studio
generators require special handling to work with `ccache`.

### Special handling for Xcode

When using the Xcode generator, the `CMAKE_<LANG>_COMPILER_LAUNCHER` variables are ignored.

Instead, dedicated launcher scripts will be created for each language that will wrap the actual compiler call with
`ccache` and forward all arguments.

These scripts are passed to the Xcode generator using the `CMAKE_XCODE_ATTRIBUTE_<VAR>` variables, effectively replacing
the compiler and linker commands with the launcher scripts.

### Special handling for Visual Studio

When using the Visual Studio generator, the `CMAKE_<LANG>_COMPILER_LAUNCHER` variables are ignored as well.  
Other than with Xcode, replacing the compiler and linker commands with launcher scripts does not work reliably with
Visual Studio / MSVC.

Therefore, another mode of integration, that is supported by `ccache`, is used:  
When run under a different name, `ccache` will act as a wrapper for the actual compiler, specified by the binary name,
forwarding all arguments to it.

Consequently, the module will create a copy of the `ccache` binary with the name of the actual compiler, i.e. `cl.exe`,
in the build directory and tell Visual Studio to use this copy as the compiler.

When doing so, the module also addresses a specific issue with `ccache` installed via chocolatey on Windows, where the
`ccache` binary is a shim that forwards to the actual `ccache` binary.  
This shim does not work correctly with Visual Studio, so the module will obtain the actual `ccache` binary and copy it
instead.

Another issue with `ccache` on Windows is that it does not support the `/Zi` flag, which is generally used to build with
debug information.  
However, passing `/Z7` instead of `/Zi` does work with `ccache`. It still enables generation of debug information, but
in a different format.

Therefore, the module will replace `/Zi` with `/Z7` in the global `CMAKE_<LANG>_FLAGS_<CONFIG>` variables, if present.  
Additionally, the function `ccache_fix_target_compile_options()` can be used to replace `/Zi` with `/Z7` on a per-target
basis.

### Module contents

#### Defined cache variables:

| Variable Name                | Description                                        | Default Value | ENV |
|------------------------------|----------------------------------------------------|---------------|-----|
| `CCACHE_PROGRAM`             | Path to the ccache binary                         | _(auto-detected)_ | No  |

#### Public functions / macros:

| Function / Macro | Name                                                | Description                                                                                   |
|------------------|-----------------------------------------------------|-----------------------------------------------------------------------------------------------|
| Function         | `ccache_fix_target_compile_options(TARGET)`         | Replace `/Zi` with `/Z7` in the compile options of the given target                            |

### Example usage:

```cmake
# Top-level CMakeLists.txt
cmake_minimum_required(VERSION 3.19)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
# ...
project(MyProject LANGUAGES CXX C)
enable_language(OBJC)
include(CCache)
# ...
add_executable(my_target main.cpp)
ccache_fix_target_compile_options(my_target)
```

## `ClangFormatCheck.cmake`

The [`ClangFormatCheck`](../cmake/ClangFormatCheck.cmake) module provides a way to enable clang-format based linting
checks for a given target.  
The target must be a CMake target that has sources and/or interface sources.

It will run [`clang-format`](https://clang.llvm.org/docs/ClangFormat.html) on all sources of the target and its linked
interface sources and report any formatting issues.  
If no issues are found, the target is considered to have passed the check. Otherwise, the build will fail.

Findings are output to the console in the standard clang error format.  
This makes it possible to use a tool like [`clang_tidy_converter`](https://github.com/yuriisk/clang-tidy-converter) to
convert the output to a format that can be consumed by GitLab CI's code quality features (i.e. Code Climate compatible
JSON).  
This allows for a more detailed presentation of the findings in the GitLab UI, e.g. showing findings in the MergeRequest
UI.

Formatting rules are taken from a `.clang-format` file in the project source directory.
- See the [.clang-format](../.clang-format) file in this repository for an example configuration.  
- See the [Clang-Format documentation](https://clang.llvm.org/docs/ClangFormatStyleOptions.html) for more information on
  how to configure the formatting rules.

The cache variable `CLANG_FORMAT_CHECK_PATH_BLACKLIST` can be used to exclude certain paths from being checked by
clang-format.

### Module contents

#### Defined cache variables:

| Variable Name                       | Description                                        | Default Value         | ENV |
|-------------------------------------|----------------------------------------------------|-----------------------|-----|
| `CLANG_FORMAT_CHECK_ENABLED`        | Enable clang-format based linting                  | `OFF`                 | Yes |
| `CLANG_FORMAT_CHECK_PATH_BLACKLIST` | List of path prefixes to exclude from clang-format | `${CMAKE_BINARY_DIR}` | No  |
| `CLANG_FORMAT_PROGRAM`              | Path to the clang-format binary                    | _(auto-detected)_     | No  |

Variables marked with `ENV` can also be set for a fresh CMake configuration (only) by defining an environment variable
with the same name. This is useful for e.g. CI configurations to provide flexibility and increase reusability.

#### Public functions / macros:

| Function / Macro | Name                                                | Description                                                                                  |
|------------------|-----------------------------------------------------|----------------------------------------------------------------------------------------------|
| Function         | `clang_format_check_activate_if_configured(TARGET)` | Enable clang-format checks for the given target if `CLANG_FORMAT_CHECK_ENABLED` is set to on |
| Function         | `clang_format_check_activate(TARGET)`               | Enable clang-format checks for the given target                                              |

#### Added targets:

| Target Name              | Description                                                                                                                                         |
|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| `${TARGET}-clang-format` | Created for each target for which clang-format checks are enabled. Runs clang-format on all sources of the target and its linked interface sources. |  
| `clang-format`           | A custom target that depends on all `${TARGET}-clang-format` targets, for running all clang-format checks at once.                                  |

### Dependencies:

- CMake 3.12 or newer
- [LocalUtils.cmake](../cmake/LocalUtils.cmake)

### Example usage:

```cmake
# Top-level CMakeLists.txt
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
include(ClangFormat)
# ... project setup ...
add_executable(my_target main.cpp)
clang_format_check_activate_if_configured(my_target)
```
```shell
cmake -S . -B build -DCLANG_FORMAT_CHECK_ENABLED=ON 
cmake --build build --target clang-format                     \
    | python3 -m clang_tidy_converter cc -l -j                \
    | sed -e 's;"severity": "critical";"severity": "info";'   \
    > clang-format.json
```

## `ClangTidy.cmake`

The [`ClangTidy`](../cmake/ClangTidy.cmake) module provides a way to enable clang-tidy based static analysis for a given
target.

It uses CMake's built-in support for running [`clang-tidy`](https://clang.llvm.org/extra/clang-tidy) while building a
target.  
All direct sources of the target, as well as any sources from linked interface libraries are checked.

If no issues are found, the target is considered to have passed the check. Otherwise, the build will fail.
Findings are reported alongside compiler diagnostics in the build output.

This makes it possible to use a tool like [`clang_tidy_converter`](https://github.com/yuriisk/clang-tidy-converter) to
convert the output to a format that can be consumed by GitLab CI's code quality features (i.e. Code Climate compatible
JSON).  
This allows for a more detailed presentation of the findings in the GitLab UI, e.g. showing findings in the MergeRequest
UI.

Checks are configured using a `.clang-tidy` file in the project source directory.
- See the [.clang-tidy](../.clang-tidy) file in this repository for an example configuration.
- See the [Clang-Tidy documentation](https://clang.llvm.org/extra/clang-tidy) for more information on how to configure
  the checks.

The cache variable `CLANG_TIDY_PATH_BLACKLIST` can be used to exclude certain paths from being checked by clang-tidy.

### Module contents

#### Defined cache variables:

| Variable Name                | Description                                        | Default Value         | ENV  |
|------------------------------|----------------------------------------------------|-----------------------|------|
| `CLANG_TIDY_CHECK_ENABLED`   | Enable clang-tidy based static analysis            | `OFF`                 | Yes  |
| `CLANG_TIDY_PATH_BLACKLIST`  | List of path prefixes to exclude from clang-tidy   | `${CMAKE_BINARY_DIR}` | No   |
| `CLANG_TIDY_PROGRAM`         | Path to the clang-tidy binary                      | _(auto-detected)_     | No   |

Variables marked with `ENV` can also be set for a fresh CMake configuration (only) by defining an environment variable
with the same name. This is useful for e.g. CI configurations to provide flexibility and increase reusability.

#### Public functions / macros:

| Function / Macro | Name                                                | Description                                                                                   |
|------------------|-----------------------------------------------------|-----------------------------------------------------------------------------------------------|
| Function         | `clang_tidy_activate_if_enabled(TARGET)`            | Enable clang-tidy checks for the given target if `CLANG_TIDY_CHECK_ENABLED` is set to on       |
| Function         | `clang_tidy_activate(TARGET)`                       | Enable clang-tidy checks for the given target                                                |

### Dependencies:

- CMake 3.12 or newer
- [LocalUtils.cmake](../cmake/LocalUtils.cmake)

### Example usage:

```cmake
# Top-level CMakeLists.txt
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
include(ClangTidy)
# ... project setup ...
add_executable(my_target main.cpp)
clang_tidy_activate_if_enabled(my_target)
```
```shell
cmake -S . -B build -DCLANG_TIDY_CHECK_ENABLED=ON
cmake --build build | python3 -m clang_tidy_converter cc -l -j > clang-tidy.json
```
