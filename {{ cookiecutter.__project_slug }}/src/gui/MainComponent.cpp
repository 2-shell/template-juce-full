#include "MainComponent.h"
#include <gitversion.h>
{% if cookiecutter.target_gui_add_data_target %}
#include <BinaryData.h>
{% endif %}

namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_gui_namespace }} {

//==============================================================================
MainComponent::MainComponent ()
{
    setSize (600, 400);
    {% if cookiecutter.target_gui_add_data_target %}
    juceLogoComponent_.setImage (juce::ImageCache::getFromMemory (data::juce_logo_png, data::juce_logo_pngSize));
    juceLogoComponent_.setSize (40, 40);
    juceLogoComponent_.toBack ();
    juceLogoComponent_.setCentrePosition (getWidth () / 2, 28);
    addAndMakeVisible (juceLogoComponent_);
    {% endif %}
    {% if cookiecutter.cmake_fetch_melatonin_inspector %}
    inspector_.setVisible (true);
    inspector_.toggle (true);
    {% endif %}
}

//==============================================================================
void MainComponent::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel ().findColour (juce::ResizableWindow::backgroundColourId));

    g.setFont (juce::FontOptions (16.0f));
    g.setColour (juce::Colours::white);
    const juce::String str = "Hello, JUCE! (Version: " + GitVersion::FullSemVer.as<juce::String> () + ")";
    g.drawText (str, getLocalBounds (), juce::Justification::centred, true);
}

void MainComponent::resized ()
{
    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
    {% if cookiecutter.target_gui_add_data_target %}
    juceLogoComponent_.setCentrePosition (getWidth () / 2, 28);
    {% endif %}
}

} // namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_gui_namespace }}