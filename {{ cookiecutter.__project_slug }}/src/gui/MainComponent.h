#pragma once

{% if cookiecutter.target_gui_add_juceheader %}
// If you need to remain compatible with Projucer-generated builds, and
// have called `juce_generate_juce_header(<thisTarget>)` in your CMakeLists.txt,
// you could `#include <JuceHeader.h>` here, to make all your module headers visible.
#include <JuceHeader.h>
{% else %}
// CMake builds don't use an AppConfig.h, so it's safe to include juce module headers
// directly.
#include <juce_gui_extra/juce_gui_extra.h>
{% endif %}
{% if cookiecutter.cmake_fetch_melatonin_inspector %}
#include <melatonin_inspector/melatonin_inspector.h>
{% endif %}

namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_gui_namespace }} {

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent final : public juce::Component
{
  public:
    //==============================================================================
    MainComponent ();

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized () override;

  private:
    //==============================================================================
    // Your private member variables go here...
    {% if cookiecutter.target_gui_add_data_target %}
    juce::ImageComponent juceLogoComponent_;
    {% endif %}
    {% if cookiecutter.cmake_fetch_melatonin_inspector %}
    melatonin::Inspector inspector_ { *this };
    {% endif %}

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};

} // namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_gui_namespace }}