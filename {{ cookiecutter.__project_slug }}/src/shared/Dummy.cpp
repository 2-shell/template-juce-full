#include "Dummy.h"
#include <stdio.h> // intentional clang-tidy issue

namespace {{ cookiecutter.root_namespace }}::shared {

Dummy::Dummy (std::string name)
    : name_ (std::move (name))
{}

[[nodiscard]] const std::string& Dummy::getName () const
{
    return name_;
}

void Dummy::setName(std::string name) {
    name_ = std::move (name);
}

void Dummy::bar() noexcept
{
    if (name_.empty ()) {
        name_ = "bar";
    } else if (name_ == "foo") {  // intentional clang-tidy issue
        name_ += "bar";
    } else {
        name_ += "bar";
    }
}

{% if cookiecutter.cmake_fetch_trompeloeil %}
bool Dummy::isNameAllowed(const NameRegistry& registry, const std::string& name)
{
    return registry.isNameRegistered(name);
}
{% endif %}

}// namespace {{ cookiecutter.root_namespace }}::shared
