#pragma once
#ifndef JUCE_TEMPLATE_FULL_SHARED_DUMMY_H
#define JUCE_TEMPLATE_FULL_SHARED_DUMMY_H 1

#include <string>

namespace {{ cookiecutter.root_namespace }}::shared {

{% if cookiecutter.cmake_fetch_trompeloeil %}
class NameRegistry
{
  public:
    virtual ~NameRegistry() = default;
    virtual bool isNameRegistered(const std::string& name) const = 0;
};
{% endif %}

class Dummy
{
  public:
    explicit Dummy (std::string name);
    ~Dummy () = default;

    [[nodiscard]] const std::string& getName () const;
    void setName (std::string name);

    void bar() noexcept;

    {% if cookiecutter.cmake_fetch_trompeloeil %}
    static bool isNameAllowed(const NameRegistry& registry, const std::string& name);
    {% endif %}

  private:
    std::string name_;
};

}// namespace {{ cookiecutter.root_namespace }}::shared

#endif//JUCE_TEMPLATE_FULL_SHARED_DUMMY_H
