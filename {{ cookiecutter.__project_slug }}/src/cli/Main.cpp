{% if cookiecutter.target_cli_add_juceheader %}
#include <JuceHeader.h>
{% else %}
{%if cookiecutter.target_cli_add_data_target %}
#include <BinaryData.h>
{% endif %}
#include <juce_core/juce_core.h>
{% endif %}
#include <iostream>
#include <gitversion.h>

namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_cli_namespace }} {

static int main (int argc, char* argv[])
{
    juce::ignoreUnused (argc, argv);
    const juce::String str = "Hello, JUCE! (Version: " + GitVersion::FullSemVer.as<juce::String> () + ")";

    std::cout << str << std::endl;
    std::cout << "C++ Standard: " << __cplusplus << std::endl;
#ifdef CI_BUILD
    std::cout << "Gitlab Pipeline ID: " << GitVersion::GitlabCiPipelineId << std::endl;
#endif // CI_BUILD

    {% if cookiecutter.target_cli_add_data_target %}
    std::cout << "{{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_cli_namespace }}::data::juce_logo_pngSize: ";
    std::cout << {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_cli_namespace }}::data::juce_logo_pngSize << std::endl;
    {% endif %}

    return 0;
}

} // namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_cli_namespace }}

int main (int argc, char* argv[])
{
    return {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_cli_namespace }}::main (argc, argv);
}
