# {{ cookiecutter.target_cli_name }} CMakeLists.txt

# `juce_add_console_app` adds an executable target with the name passed as the first argument
# ({{ cookiecutter.target_cli_name }} here). This target is a normal CMake target, but has a lot of extra properties
# set up by default. This function accepts many optional arguments. Check the readme at
# `docs/CMake API.md` in the JUCE repo for the full list.

juce_add_console_app({{ cookiecutter.__target_cli_cmake_identifier }}
    PRODUCT_NAME "{{ cookiecutter.target_cli_name }}")     # The name of the final executable, which can differ from the target name

{% if cookiecutter.target_cli_add_juceheader %}
# `juce_generate_juce_header` will create a JuceHeader.h for a given target, which will be generated
# into the build tree. This header should be included with `#include <JuceHeader.h>`. The include
# path for this header will be automatically added to the target. The main function of the
# JuceHeader is to include all the JUCE module headers for a particular target; if you're happy to
# include module headers directly, you probably don't need to call this.

juce_generate_juce_header({{ cookiecutter.__target_cli_cmake_identifier }})
{% endif %}

# `target_sources` adds source files to a target. We pass the target that needs the sources as the
# first argument, then a visibility parameter for the sources which should normally be PRIVATE.
# Finally, we supply a list of source files that will be built into the target. This is a standard
# CMake command.

target_sources({{ cookiecutter.__target_cli_cmake_identifier }}
    PRIVATE
    Main.cpp
)

# `target_compile_definitions` adds some preprocessor definitions to our target. In a Projucer
# project, these might be passed in the 'Preprocessor Definitions' field. JUCE modules also make use
# of compile definitions to switch certain features on/off, so if there's a particular feature you
# need that's not on by default, check the module header for the correct flag to set here. These
# definitions will be visible both to your code, and also the JUCE module code, so for new
# definitions, pick unique names that are unlikely to collide! This is a standard CMake command.

target_compile_definitions({{ cookiecutter.__target_cli_cmake_identifier }}
    PRIVATE
    # JUCE_WEB_BROWSER and JUCE_USE_CURL would be on by default, but you might not need them.
    JUCE_WEB_BROWSER=0  # If you remove this, add `NEEDS_WEB_BROWSER TRUE` to the `juce_add_console_app` call
    JUCE_USE_CURL=0     # If you remove this, add `NEEDS_CURL TRUE` to the `juce_add_console_app` call
)

{% if cookiecutter.target_cli_add_data_target %}
# If the target needs extra binary assets, they can be added here. The first argument is the name of
# a new static library target that will include all the binary resources. There is an optional
# `NAMESPACE` argument that can specify the namespace of the generated binary data class. Finally,
# the SOURCES argument should be followed by a list of source files that should be built into the
# static library. These source files can be of any kind (wav data, images, fonts, icons etc.).
# Conversion to binary-data will happen when the target is built.

juce_add_binary_data({{ cookiecutter.__target_cli_cmake_identifier }}Data
    NAMESPACE {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_cli_namespace }}::data
    SOURCES
    ../../resources/juce_logo.png
)
{% endif %}

# `target_link_libraries` links libraries and JUCE modules to other libraries or executables. Here,
# we're linking our executable target to the `juce::juce_core` module. Inter-module dependencies are
# resolved automatically. If you'd generated a binary data target above, you would need to link to
# it here too. This is a standard CMake command.

target_link_libraries({{ cookiecutter.__target_cli_cmake_identifier }}
    PRIVATE
    {% if cookiecutter.target_cli_add_data_target %}
    {{ cookiecutter.__target_cli_cmake_identifier }}Data            # If you'd created a binary data target, you'd link to it here
    {% endif %}
    juce::juce_core
    {{ cookiecutter.root_namespace }}::shared
    GitVersion::GitVersion
    PUBLIC
    juce::juce_recommended_config_flags
    juce::juce_recommended_warning_flags
)

include(ClangTidy)
clang_tidy_activate_if_enabled({{ cookiecutter.__target_cli_cmake_identifier }})

juce_target_collect_artefacts({{ cookiecutter.__target_cli_cmake_identifier }} {{ '${' }}{{ cookiecutter.__cmake_var_prefix }}_ARTEFACTS_DIR{{ '}' }})
