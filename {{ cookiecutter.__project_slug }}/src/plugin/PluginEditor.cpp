#include "PluginEditor.h"
#include "PluginProcessor.h"
#include <gitversion.h>
{% if cookiecutter.target_plugin_add_data_target %}
#include <BinaryData.h>
{% endif %}

namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_plugin_namespace }} {

//==============================================================================
AudioPluginAudioProcessorEditor::AudioPluginAudioProcessorEditor (AudioPluginAudioProcessor& p)
    : AudioProcessorEditor (&p)
    , processorRef (p)
{
    juce::ignoreUnused (processorRef);
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);

    {% if cookiecutter.target_plugin_add_data_target %}
    juceLogoComponent_.setImage (juce::ImageCache::getFromMemory (data::juce_logo_png, data::juce_logo_pngSize));
    juceLogoComponent_.setSize (40, 40);
    juceLogoComponent_.toBack ();
    juceLogoComponent_.setCentrePosition (getWidth () / 2, 28);
    addAndMakeVisible (juceLogoComponent_);
    {% endif %}
    {% if cookiecutter.cmake_fetch_melatonin_inspector %}
    inspector_.setVisible (true);
    inspector_.toggle (true);
    {% endif %}
}

AudioPluginAudioProcessorEditor::~AudioPluginAudioProcessorEditor () = default;

//==============================================================================
void AudioPluginAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel ().findColour (juce::ResizableWindow::backgroundColourId));

    g.setColour (juce::Colours::white);
    g.setFont (15.0f);
    const juce::String str = "Hello, JUCE! (Version: " + GitVersion::FullSemVer.as<juce::String>() + ")";
    g.drawFittedText (str, getLocalBounds (), juce::Justification::centred, 1);
}

void AudioPluginAudioProcessorEditor::resized ()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    {% if cookiecutter.target_plugin_add_data_target %}
    juceLogoComponent_.setCentrePosition (getWidth () / 2, 28);
    {% endif %}
}

} // namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_plugin_namespace }}