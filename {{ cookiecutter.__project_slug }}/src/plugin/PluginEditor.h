#pragma once

#include "PluginProcessor.h"
{% if cookiecutter.cmake_fetch_melatonin_inspector %}
#include <melatonin_inspector/melatonin_inspector.h>
{% endif %}

namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_plugin_namespace }} {

//==============================================================================
class AudioPluginAudioProcessorEditor final : public juce::AudioProcessorEditor
{
  public:
    explicit AudioPluginAudioProcessorEditor (AudioPluginAudioProcessor&);
    ~AudioPluginAudioProcessorEditor () override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized () override;

  private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    AudioPluginAudioProcessor& processorRef;

    {% if cookiecutter.target_plugin_add_data_target %}
    juce::ImageComponent juceLogoComponent_;
    {% endif %}
    {% if cookiecutter.cmake_fetch_melatonin_inspector %}
    melatonin::Inspector inspector_ { *this };
    {% endif %}

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioPluginAudioProcessorEditor)
};

} // namespace {{ cookiecutter.root_namespace }}::{{ cookiecutter.__target_plugin_namespace }}