include(LocalUtils)

add_subdirectory(shared)

if ({{ cookiecutter.__cmake_var_prefix }}_BUILD_MAIN_TARGETS)
    MESSAGE(STATUS "Checking main targets...")
    list(APPEND CMAKE_MESSAGE_INDENT ": ")

    {% if cookiecutter.target_cli %}
    add_subdirectory_if_enabled(cli "Commandline App" ON)
    {% endif %}
    {% if cookiecutter.target_gui %}
    add_subdirectory_if_enabled(gui "Gui App" ON)
    {% endif %}
    {% if cookiecutter.target_plugin %}
    add_Subdirectory_if_enabled(plugin "Plugin" ON)
    {% endif %}

    list(POP_BACK CMAKE_MESSAGE_INDENT)
endif ()
