cmake_minimum_required(VERSION 3.25...3.29)

if(CMAKE_SCRIPT_MODE_FILE)
    if(NOT BUILD_DIR)
        message(FATAL_ERROR "BUILD_DIR is not set")
    endif()
    if(NOT CMAKE_BUILD_TYPE)
        message(FATAL_ERROR "CMAKE_BUILD_TYPE is not set")
    endif()
    if(NOT DEFINED ENV{CI_PROJECT_DIR})
        message(FATAL_ERROR "CI_PROJECT_DIR environment variable is not set")
    endif()
    {% if cookiecutter._project_template_gitlab_ci_test_build %}
    if(NOT DEFINED ENV{SRC_DIR})
        message(FATAL_ERROR "SRC_DIR environment variable is not set")
    endif()
    {% endif %}
    message(STATUS "Running clang-tidy enabled build...")
    execute_process(
            COMMAND ${CMAKE_COMMAND} --build ${BUILD_DIR} --config ${CMAKE_BUILD_TYPE} -j
            COMMAND python3 -m clang_tidy_converter cc -l -j
            {% if cookiecutter._project_template_gitlab_ci_test_build %}
            COMMAND sed -e "s;$ENV{CI_PROJECT_DIR}/$ENV{SRC_DIR}/*;{{ '{{ cookiecutter.__project_slug }}' }}/;"
            {% else %}
            COMMAND sed -e "s;$ENV{CI_PROJECT_DIR}/*;;"
            {% endif %}
            OUTPUT_FILE clang-tidy.json
            ERROR_FILE clang-tidy.json
            COMMAND_ECHO STDERR
            COMMAND_ERROR_IS_FATAL ANY
    )
endif()