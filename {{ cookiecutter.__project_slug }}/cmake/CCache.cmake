# CCache support for CMake
#
# Copyright (c) 2024 2-Shell (https://gitlab.com/2-shell/template-juce-full)
# License: MIT (https://gitlab.com/2-shell/template-juce-full/-/blob/main/LICENSE)
#
# This module will attempt to locate ccache on the system path and configure
# the project to use it if found, including special handling for Xcode and
# Visual Studio generators.
#
# It also provides a function ccache_fix_target_compile_options() that can be
# used to fix compile options that might interfere with ccache for a given target.
#
# Usage:
# Simply include this file in your CMakeLists.txt file (preferably shortly after
# the top-level project() call).
# All necessary configuration will be done automatically.
#
# When needed, call the function ccache_fix_target_compile_options() on each
# target that you want to fix compile options for.
#

find_program(CCACHE_PROGRAM ccache DOC "Path to the ccache binary")

if(CCACHE_PROGRAM)
    message(STATUS "Using CCache: ${CCACHE_PROGRAM}")

    if(CMAKE_GENERATOR STREQUAL "Xcode")
        set(CCACHE_XC_CC_LAUNCHER "${CMAKE_BINARY_DIR}/xc-launch-cc.sh")
        file(CONFIGURE OUTPUT "${CCACHE_XC_CC_LAUNCHER}"
                CONTENT "#!/usr/bin/env sh\nexec \"@CCACHE_PROGRAM@\" \"@CMAKE_C_COMPILER@\" \"$@\"\n"
                ESCAPE_QUOTES @ONLY
        )
        set(CCACHE_XC_CXX_LAUNCHER "${CMAKE_BINARY_DIR}/xc-launch-cxx.sh")
        file(CONFIGURE OUTPUT "${CCACHE_XC_CXX_LAUNCHER}"
                CONTENT "#!/usr/bin/env sh\nexec \"@CCACHE_PROGRAM@\" \"@CMAKE_CXX_COMPILER@\" \"$@\"\n"
                ESCAPE_QUOTES @ONLY
        )
        file(CHMOD "${CCACHE_XC_CC_LAUNCHER}" "${CCACHE_XC_CXX_LAUNCHER}"
                PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

        set(CMAKE_XCODE_ATTRIBUTE_CC "${CCACHE_XC_CC_LAUNCHER}")
        set(CMAKE_XCODE_ATTRIBUTE_CXX "${CCACHE_XC_CXX_LAUNCHER}")
        set(CMAKE_XCODE_ATTRIBUTE_LD "${CCACHE_XC_CC_LAUNCHER}")
        set(CMAKE_XCODE_ATTRIBUTE_LDPLUSPLUS "${CCACHE_XC_CXX_LAUNCHER}")

    elseif(MSVC OR CMAKE_GENERATOR MATCHES "Visual Studio")

        message(STATUS "Fixing any global compiler flags that might interfere with ccache")
        foreach(config DEBUG RELWITHDEBINFO)
            foreach(lang C CXX)
                set(flags_var "CMAKE_${lang}_FLAGS_${config}")
                string(REPLACE "/Zi" "/Z7" ${flags_var} "${${flags_var}}")
                set(${flags_var} "${${flags_var}}")
            endforeach()
        endforeach()

        set(CCACHE_PATH_ "${CCACHE_PROGRAM}")
        # Check if found ccache binary is a shim (e.g. installed through chocolatey)
        message(CHECK_START "Checking if ccache is a shim")
        execute_process(COMMAND ${CCACHE_PROGRAM} --shimgen-noop OUTPUT_VARIABLE CCACHE_SHIMGEN_OUTPUT)
        if(${CCACHE_SHIMGEN_OUTPUT} MATCHES "\[shim\].*path to executable: ([^\n]+)")
            set(CCACHE_PATH_ "${CMAKE_MATCH_1}")
            message(CHECK_PASS "yes. Original path: ${CCACHE_PATH_}")
        else()
            message(CHECK_PASS "no")
        endif()

        # Copy ccache to the build directory to have Visual Studio use it
        message(STATUS "Creating CL alias from ccache binary '${CCACHE_PATH_}' and tell VisualStudio to use it")
        file(COPY_FILE ${CCACHE_PATH_} ${CMAKE_BINARY_DIR}/cl.exe ONLY_IF_DIFFERENT)
        set(CMAKE_VS_GLOBALS
                "CLToolExe=cl.exe"
                "CLToolPath=${CMAKE_BINARY_DIR}"
                "TrackFileAccess=false"
                "UseMultiToolTask=true"
                "DebugInformationFormat=OldStyle"
        )

        # By default Visual Studio generators will use /Zi which is not compatible
        # with ccache, so tell Visual Studio to use /Z7 instead.
        message(STATUS "Setting MSVC debug information format to 'Embedded'")
        set(CMAKE_MSVC_DEBUG_INFORMATION_FORMAT "$<$<CONFIG:Debug,RelWithDebInfo>:Embedded>")
    endif()

    set(CMAKE_C_COMPILER_LAUNCHER "${CCACHE_PROGRAM}")
    set(CMAKE_CXX_COMPILER_LAUNCHER "${CCACHE_PROGRAM}")
    set(CMAKE_OBJC_COMPILER_LAUNCHER "${CCACHE_PROGRAM}")
    set(CMAKE_OBJCXX_COMPILER_LAUNCHER "${CCACHE_PROGRAM}")
    set(CMAKE_CUDA_COMPILER_LAUNCHER "${CCACHE_PROGRAM}") # CMake 3.9+

else()

    message(STATUS "Not using CCache")

endif()

function(ccache_fix_target_compile_options target)
    if(CCACHE_PROGRAM AND MSVC)
        message(STATUS "Fixing compile options that might interfere with ccache for target ${target}")
        get_target_property(compile_options_ ${target} COMPILE_OPTIONS)
        get_target_property(interface_compile_options_ ${target} INTERFACE_COMPILE_OPTIONS)
        string(REPLACE "/Zi" "/Z7" compile_options_ "${compile_options_}")
        string(REPLACE "/Zi" "/Z7" interface_compile_options_ "${interface_compile_options_}")
        set_target_properties(${target} PROPERTIES COMPILE_OPTIONS "${compile_options_}")
        set_target_properties(${target} PROPERTIES INTERFACE_COMPILE_OPTIONS "${interface_compile_options_}")
    endif()
endfunction()