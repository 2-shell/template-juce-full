# CMake module for finding JUCE in various ways
# This module is intended to be used in a CMake project to find or fetch JUCE and make it available as a target.
#
# It provides a single function `find_JUCE` that should be called in the root CMakeLists.txt file of the project.
# The function will attempt to find JUCE in the following order:
#   1. Using the FIND_JUCE_ROOT variable (if set) to look for an installed JUCE package at the specified location (i.e.
#      a directory containing a JUCEConfig.cmake file, e.g. by running `cmake --install` in a JUCE project)
#   2. Using the FIND_JUCE_ROOT variable (if set) to look for a global working copy of JUCE at the specified location
#   3. If FIND_JUCE_PREFER_FETCH is set to OFF, using a fuzzy search to look for a local working copy of JUCE in the
#      project's source directory and/or its parent directory. Project subdirectories are preferred over parent
#      directories. The search is fuzzy in the sense that it tries some common directory names and suffixes.
#   4. Fetching JUCE from the internet using FetchContent, if all methods above did not succeed.
#
# Generally, the last option is the most reliable and should be preferred.
# Not least because it allows to specify the exact version of JUCE to fetch and reduces overhead when using submodules
# that also depend on JUCE, since it avoids having to fetch and configure JUCE multiple times.
#
# All other options are provided for convenience and to support existing projects.
#
# The function passes any arguments it receives to FetchContent_Declare when fetching JUCE.
# This allows to specify additional options such as the GIT_REPOSITORY, GIT_TAG, etc.
# See the FetchContent documentation for more information.
#
# Example usage:
#   find_JUCE(GIT_REPOSITORY https://github.com/juce-framework/JUCE.git GIT_TAG 7.0.9 GIT_SHALLOW TRUE)
#
# The function will set the JUCE_FOUND variable to TRUE if JUCE was found or fetched, and to FALSE otherwise.
# If JUCE was found or fetched, the function will also add the JUCE target to the current CMake project.
#
# CMake variables:
#   - FIND_JUCE_ROOT: Path to the JUCE root directory. If set, the function will first look for an installed JUCE
#                     package or working copy at the specified location.
#   - FIND_JUCE_PREFER_FETCH: If set to ON (default), the function will prefer fetching JUCE from the internet if no
#                             FIND_JUCE_ROOT is set or if the specified location does not contain a JUCE package or
#                             working copy. If set to OFF, the function will first look for a local working copy of JUCE
#                             in the project's source directory and/or its parent directory before fetching JUCE.
#

include_guard(GLOBAL)
include(FetchContent)

set(FIND_JUCE_ROOT "" CACHE PATH "Path to JUCE root directory")
option(FIND_JUCE_PREFER_FETCH "Prefer fetching JUCE from the internet" ON)

function(find_JUCE)
    if(PROJECT_NAME)
        message(CHECK_START "Resolving JUCE for ${PROJECT_NAME}")
    else()
        message(CHECK_START "Resolving JUCE")
    endif()
    list(APPEND CMAKE_MESSAGE_INDENT ": ")
    set(JUCE_FOUND FALSE)

    if(FIND_JUCE_ROOT)
        message(STATUS "Using JUCE_ROOT: ${FIND_JUCE_ROOT}")

        if(NOT JUCE_FOUND)
            find_JUCE_check_package(${FIND_JUCE_ROOT})
        endif()

        if(NOT JUCE_FOUND)
            find_JUCE_check_global_repo(${FIND_JUCE_ROOT})
        endif()

    endif()

    if(NOT JUCE_FOUND AND NOT FIND_JUCE_PREFER_FETCH)
        find_JUCE_check_local_repo(${CMAKE_SOURCE_DIR})
    endif()

    if(NOT JUCE_FOUND)
        cmake_parse_arguments(PARSE_ARGV 0 FWD "" "" "")
        find_JUCE_fetch_juce(${FWD_UNPARSED_ARGUMENTS})
    endif()

    list(POP_BACK CMAKE_MESSAGE_INDENT)
    if(JUCE_FOUND)
        message(CHECK_PASS "done")
    else()
        message(CHECK_FAIL "failed")
        message(FATAL_ERROR "JUCE could not be found or downloaded! Please set FIND_JUCE_ROOT to the path of your JUCE installation.")
    endif()
endfunction()

########################################################################################################################
# Internal functions
########################################################################################################################

function(find_JUCE_check_package JUCE_ROOT)
    message(CHECK_START "Looking for JUCE package in ${JUCE_ROOT}")

    list(APPEND CMAKE_MESSAGE_INDENT ": ")
    find_package(JUCE CONFIG PATHS ${JUCE_ROOT} QUIET)
    list(POP_BACK CMAKE_MESSAGE_INDENT)

    if(JUCE_FOUND)
        message(CHECK_PASS "found")
    else()
        message(CHECK_FAIL "not found")
    endif()

    return(PROPAGATE JUCE_FOUND)
endfunction()

function(find_JUCE_check_global_repo JUCE_ROOT)
    message(CHECK_START "Looking for JUCE in repo at ${JUCE_ROOT}")
    set(JUCE_FOUND FALSE)

    if(EXISTS ${JUCE_ROOT}/modules/juce_core/juce_core.h)
        set(JUCE_FOUND TRUE)
    endif()

    if(JUCE_FOUND)
        list(APPEND CMAKE_MESSAGE_INDENT ": ")
        add_subdirectory(${JUCE_ROOT} JUCE EXCLUDE_FROM_ALL)
        list(POP_BACK CMAKE_MESSAGE_INDENT)
        message(CHECK_PASS "found")
    else()
        message(CHECK_FAIL "not found")
    endif()

    return(PROPAGATE JUCE_FOUND)
endfunction()

function(find_JUCE_check_local_repo REPO_ROOT)
    message(CHECK_START "Looking for JUCE in ${REPO_ROOT}")
    set(JUCE_FOUND FALSE)

    list(APPEND CMAKE_MESSAGE_INDENT ": ")
    find_JUCE_get_juce_search_path_candidates(juce_search_path_candidates ${REPO_ROOT})
    find_JUCE_get_juce_dir_name_candidates(juce_dir_name_candidates)
    find_path(JUCE_ROOT_DIR modules/juce_core/juce_core.h
            HINTS ${juce_search_path_candidates}
            PATH_SUFFIXES ${juce_dir_name_candidates}
            NO_CACHE
    )
    list(POP_BACK CMAKE_MESSAGE_INDENT)

    if(JUCE_ROOT_DIR)
        set(JUCE_FOUND TRUE)
    endif()

    if(JUCE_FOUND)
        message(CHECK_PASS "found at '${JUCE_ROOT_DIR}'")
        add_subdirectory(${JUCE_ROOT_DIR} JUCE EXCLUDE_FROM_ALL)
    else()
        message(CHECK_FAIL "not found")
    endif()

    return(PROPAGATE JUCE_FOUND)
endfunction()

function(find_JUCE_fetch_juce)
    message(CHECK_START "Fetching JUCE for ${PROJECT_NAME}")

    cmake_parse_arguments(PARSE_ARGV 0 FWD "" "" "")
    FetchContent_Declare(JUCE ${FWD_UNPARSED_ARGUMENTS})
    list(APPEND CMAKE_MESSAGE_INDENT ": ")
    FetchContent_MakeAvailable(JUCE)
    set_property(DIRECTORY ${JUCE_SOURCE_DIR} PROPERTY EXCLUDE_FROM_ALL TRUE)
    list(POP_BACK CMAKE_MESSAGE_INDENT)
    set(JUCE_FOUND TRUE)

    message(CHECK_PASS "done")
    return(PROPAGATE JUCE_FOUND)
endfunction()

function(find_JUCE_get_juce_search_path_candidates out_var local_dir)
    set(${out_var})
    set(scopes "${local_dir}" "${local_dir}/..")
    set(suffixes SDKs sdks SDK sdk libs third_party third-party 3rd_party 3rd-party deps dependencies)

    foreach(scope ${scopes})
        list(APPEND ${out_var} "${scope}")
        foreach(suffix ${suffixes})
            list(APPEND ${out_var} "${scope}/${suffix}")
        endforeach()
    endforeach()

    return(PROPAGATE ${out_var})
endfunction()

function(find_JUCE_get_juce_dir_name_candidates out_var)
    set(${out_var}
            JUCE
            juce
            juce-official
            juce-git
    )
    return(PROPAGATE ${out_var})
endfunction()