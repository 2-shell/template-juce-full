# CMake integration for clang-tidy static analysis
#
# Copyright (c) 2024 2-Shell (https://gitlab.com/2-shell/template-juce-full)
# License: MIT (https://gitlab.com/2-shell/template-juce-full/-/blob/main/LICENSE)
#
# This module provides a way to enable clang-tidy based static analysis for a given target.
#
# It uses CMake's built-in support for running clang-tidy while building a target.
# All direct sources of the target, as well as any sources from linked interface libraries are checked.
#
# If no issues are found, the target is considered to have passed the check. Otherwise, the build will fail.
# Findings are reported alongside compiler diagnostics in the build output.
#
# Checks are configured using a `.clang-tidy` file in the project source directory.
#  - See the .clang-tidy file in this repository for an example configuration.
#  - See https://clang.llvm.org/extra/clang-tidy for more information on how to configure the checks.
#
# Usage:
# 1. Include this file in your CMakeLists.txt
# 2. Call `clang_tidy_activate_if_enabled` for each target you want to check.
# 3. Enable the `CLANG_TIDY_ENABLED` option to enable clang-tidy for all targets.
#
# Example:
# ```
# cmake_minimum_required(VERSION 3.12)
# project(MyProject)
#
# include(cmake/ClangTidy.cmake)
#
# add_executable(MyTarget main.cpp)
# clang_tidy_activate_if_enabled(MyTarget)
# ```
#
include_guard(GLOBAL)
include(${CMAKE_CURRENT_LIST_DIR}/LocalUtils.cmake)

env_option(CLANG_TIDY_ENABLED "Enable clang-tidy" OFF)

set(CLANG_TIDY_PATH_BLACKLIST "${CMAKE_BINARY_DIR}" CACHE STRING "List of path prefixes to exclude from clang-tidy")
mark_as_advanced(CLANG_TIDY_PATH_BLACKLIST)


function(clang_tidy_activate_if_enabled target)
    if(CLANG_TIDY_ENABLED)
        find_program(CLANG_TIDY_PROGRAM NAMES
                clang-tidy
                clang-tidy-20 clang-tidy-19 clang-tidy-18 clang-tidy-17 clang-tidy-16 clang-tidy-15 clang-tidy-14 clang-tidy-13
                clang-tidy-12 clang-tidy-11 clang-tidy-10 clang-tidy-9 clang-tidy-8 clang-tidy-7 clang-tidy-6.0 clang-tidy-6
                DOC "Path to the clang-tidy binary"
        )
        clang_tidy_enable(${target})
    endif()
endfunction()


function(clang_tidy_enable target)
    if(CLANG_TIDY_PROGRAM)
        message(STATUS "Enabling clang-tidy for ${target}")

        clang_tidy_get_commandline(clang_tidy_commandline ${target})

        set_target_properties(${target} PROPERTIES EXPORT_COMPILE_COMMANDS ON)
        foreach(lang C CXX OBJC OBJCXX)
            set_target_properties(${target} PROPERTIES ${lang}_CLANG_TIDY "${clang_tidy_commandline}")
        endforeach()
    else()
        message(WARNING "clang-tidy not found, skipping for ${target}")
    endif()
endfunction()


function(clang_tidy_get_commandline out_var target)
    set(${out_var} "${CLANG_TIDY_PROGRAM}")
    list(APPEND ${out_var} "-quiet")
    list(APPEND ${out_var} "-p" "${CMAKE_CURRENT_BINARY_DIR}")
    list(APPEND ${out_var} "-header-filter=${CMAKE_CURRENT_LIST_DIR}/.*")
    list(APPEND ${out_var} "-warnings-as-errors=*")

    clang_tidy_get_line_filter_argument(line_filter_argument ${target})
    list(APPEND ${out_var} "${line_filter_argument}")

    clang_tidy_get_checks_argument(checks_argument)
    list(APPEND ${out_var} "${checks_argument}")

    return(PROPAGATE ${out_var})
endfunction()


function(clang_tidy_get_line_filter_argument out_var target)
    get_target_property(target_sources ${target} SOURCES)
    get_target_property(target_interface_sources ${target} INTERFACE_SOURCES)

    clang_tidy_get_target_linked_interface_sources(target_linked_interface_sources ${target})
    clang_tidy_filter_linked_sources(filtered_linked_interface_sources "${target_linked_interface_sources}")

    set(line_filter_elements "")
    foreach(source ${target_sources} ${target_interface_sources} ${filtered_linked_interface_sources})
        if(source)
            list(APPEND line_filter_elements "{\"name\":\"${source}\"}")
        endif()
    endforeach()

    string(JOIN "," line_filter_list ${line_filter_elements})
    set(${out_var} "-line-filter=[${line_filter_list}]")

    return(PROPAGATE ${out_var})
endfunction()


function(clang_tidy_get_target_linked_interface_sources out_var target)
    set(${out_var})

    get_target_property(target_interface_sources ${target} INTERFACE_SOURCES)
    foreach(source ${target_interface_sources})
        if(source)
            list(APPEND ${out_var} ${source})
        endif()
    endforeach()

    get_target_property(target_link_libraries ${target} LINK_LIBRARIES)
    foreach(lib ${target_link_libraries})
        if(TARGET ${lib})
            clang_tidy_get_target_linked_interface_sources(sources ${lib})
            list(APPEND ${out_var} ${sources})
        endif()
    endforeach()

    return(PROPAGATE ${out_var})
endfunction()


function(clang_tidy_filter_linked_sources out_var sources)
    set(${out_var})

    foreach(source ${sources})
        if(source)
            cmake_path(IS_PREFIX CMAKE_SOURCE_DIR "${source}" NORMALIZE is_in_source_dir)
            if(is_in_source_dir)
                foreach(path ${CLANG_TIDY_PATH_BLACKLIST})
                    cmake_path(IS_PREFIX path "${source}" NORMALIZE is_in_blacklist)
                    if(NOT is_in_blacklist)
                        list(APPEND ${out_var} ${source})
                    endif()
                endforeach()
            endif()
        endif()
    endforeach()

    return(PROPAGATE ${out_var})
endfunction()


function(clang_tidy_get_checks_argument out_var)
    set(${out_var} "-checks=-*,bugprone-*,-bugprone-suspicious-include,cert-dcl21-cpp,cert-dcl58-cpp,cert-err34-c,cert-err52-cpp,cert-err60-cpp,cert-flp30-c,cert-msc50-cpp,cert-msc51-cpp,cert-str34-c,clang-analyzer-*,cppcoreguidelines-interfaces-global-init,cppcoreguidelines-narrowing-conversions,cppcoreguidelines-pro-type-member-init,cppcoreguidelines-pro-type-static-cast-downcast,cppcoreguidelines-slicing,google-default-arguments,google-explicit-constructor,google-runtime-operator,hicpp-exception-baseclass,hicpp-multiway-paths-covered,misc-misplaced-const,misc-new-delete-overloads,misc-no-recursion,misc-non-copyable-objects,misc-throw-by-value-catch-by-reference,misc-unconventional-assign-operator,misc-uniqueptr-reset-release,modernize-avoid-bind,modernize-concat-nested-namespaces,modernize-deprecated-headers,modernize-deprecated-ios-base-aliases,modernize-loop-convert,modernize-make-shared,modernize-make-unique,modernize-pass-by-value,modernize-raw-string-literal,modernize-redundant-void-arg,modernize-replace-auto-ptr,modernize-replace-disallow-copy-and-assign-macro,modernize-replace-random-shuffle,modernize-return-braced-init-list,modernize-shrink-to-fit,modernize-unary-static-assert,modernize-use-auto,modernize-use-bool-literals,modernize-use-emplace,modernize-use-equals-default,modernize-use-equals-delete,modernize-use-nodiscard,modernize-use-noexcept,modernize-use-nullptr,modernize-use-override,modernize-use-transparent-functors,modernize-use-uncaught-exceptions,mpi-buffer-deref,mpi-type-mismatch,openmp-use-default-none,performance-faster-string-find,performance-for-range-copy,performance-implicit-conversion-in-loop,performance-inefficient-algorithm,performance-inefficient-string-concatenation,performance-inefficient-vector-operation,performance-move-const-arg,performance-move-constructor-init,performance-no-automatic-move,performance-noexcept-move-constructor,performance-trivially-destructible,performance-type-promotion-in-math-fn,performance-unnecessary-copy-initialization,performance-unnecessary-value-param,readability-avoid-const-params-in-decls,readability-const-return-type,readability-container-size-empty,readability-convert-member-functions-to-static,readability-delete-null-pointer,readability-deleted-default,readability-inconsistent-declaration-parameter-name,readability-make-member-function-const,readability-misleading-indentation,readability-misplaced-array-index,readability-non-const-parameter,readability-redundant-control-flow,readability-redundant-declaration,readability-redundant-function-ptr-dereference,readability-redundant-smartptr-get,readability-redundant-string-cstr,readability-redundant-string-init,readability-simplify-subscript-expr,readability-static-accessed-through-instance,readability-static-definition-in-anonymous-namespace,readability-string-compare,readability-uniqueptr-delete-release,readability-use-anyofallof")
    return(PROPAGATE ${out_var})
endfunction()
