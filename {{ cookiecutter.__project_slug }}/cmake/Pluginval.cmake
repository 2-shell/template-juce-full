cmake_minimum_required(VERSION 3.25...3.29)
include(${CMAKE_CURRENT_LIST_DIR}/LocalUtils.cmake)

env_option(PLUGINVAL_TESTS_ENABLED "Enable pluginval tests" ON)

function(pluginval_validate plugin_path)
    cmake_path(ABSOLUTE_PATH plugin_path NORMALIZE OUTPUT_VARIABLE plugin_absolute_path)
    pluginval_fetch(PLUGINVAL_EXECUTABLE)
    find_program(VST3VALIDATOR_EXECUTABLE vst3-validator)

    set(PLUGINVAL_ARGS
            --skip-gui-tests
            --repeat 3
            --randomise
            --strictness-level 10
            --validate ${plugin_absolute_path}
            --output-dir ${CMAKE_BINARY_DIR}/reports/pluginval
    )

    message(STATUS "Validating: ${plugin_absolute_path}")
    message(STATUS "      with: ${PLUGINVAL_EXECUTABLE}")
    if(VST3VALIDATOR_EXECUTABLE)
        message(STATUS "       and: ${VST3VALIDATOR_EXECUTABLE}")
        list(APPEND PLUGINVAL_ARGS --vst3validator ${VST3VALIDATOR_EXECUTABLE})
    endif()

    if(plugin_absolute_path MATCHES ".*\\.component$")
        cmake_path(GET plugin_absolute_path FILENAME plugin_name)
        set(au_install_path "$ENV{HOME}/Library/Audio/Plug-Ins/Components")
        file(INSTALL ${plugin_absolute_path} DESTINATION "${au_install_path}" USE_SOURCE_PERMISSIONS)
        execute_process(COMMAND "killall" "-9" "AudioComponentRegistrar" COMMAND_ECHO STDERR)
    endif()

    execute_process(COMMAND ${PLUGINVAL_EXECUTABLE} ${PLUGINVAL_ARGS}
            COMMAND_ECHO STDERR RESULT_VARIABLE pluginval_result)

    if(au_install_path)
        file(REMOVE_RECURSE "${au_install_path}/${plugin_name}")
    endif()

    if(NOT pluginval_result EQUAL 0)
        message(FATAL_ERROR "Validation failed with exit code: ${pluginval_result}")
    endif()
endfunction()


function(pluginval_fetch out_var)
    # Prefer a pre-installed pluginval in the PATH, if available
    find_program(${out_var} pluginval)
    if(${out_var})
        return(PROPAGATE out_var)
    endif()

    # otherwise, download and extract the pluginval binary
    set(PLUGINVAL_VERSION 1.0.3)
    set(PLUGINVAL_DOWNLOAD_BASE_URL "https://github.com/Tracktion/pluginval/releases/download/v${PLUGINVAL_VERSION}")
    set(PLUGINVAL_ARCHIVE_MD5_Windows "054d1a2786aeb0226e4d3611fe28c766")
    set(PLUGINVAL_ARCHIVE_MD5_Linux "b8c351cfa8222a5f1a055d22cc64c5df")
    set(PLUGINVAL_ARCHIVE_MD5_macOS "82f63627013bc8978cf536436f56760e")

    pluginval_get_platform_name(PLUGINVAL_PLATFORM_NAME)
    set(PLUGINVAL_DOWNLOAD_FILE "pluginval_${PLUGINVAL_PLATFORM_NAME}.zip")
    set(PLUGINVAL_DOWNLOAD_URL "${PLUGINVAL_DOWNLOAD_BASE_URL}/${PLUGINVAL_DOWNLOAD_FILE}")
    set(PLUGINVAL_TARGET_DIR "${CMAKE_BINARY_DIR}/.pluginval-${PLUGINVAL_PLATFORM_NAME}")

    message(STATUS "Fetching pluginval for ${PLUGINVAL_PLATFORM_NAME}")
    list(APPEND CMAKE_MESSAGE_INDENT ": ")
    include(FetchContent)
    FetchContent_Populate(pluginval QUIET
            URL ${PLUGINVAL_DOWNLOAD_URL}
            URL_MD5 ${PLUGINVAL_ARCHIVE_MD5_${PLUGINVAL_PLATFORM_NAME}}
            BINARY_DIR ${PLUGINVAL_TARGET_DIR}/bin
            SOURCE_DIR ${PLUGINVAL_TARGET_DIR}/bin
            SUBBUILD_DIR ${PLUGINVAL_TARGET_DIR}/build
    )
    find_program(${out_var} pluginval REQUIRED
        PATHS ${pluginval_SOURCE_DIR} ${pluginval_SOURCE_DIR}/Contents/MacOS
    )
    list(POP_BACK CMAKE_MESSAGE_INDENT)
    return(PROPAGATE out_var)
endfunction()


function(pluginval_get_platform_name out_var)
    set(${out_var} ${CMAKE_HOST_SYSTEM_NAME})
    if(CMAKE_HOST_SYSTEM_NAME STREQUAL "Darwin")
        set(${out_var} "macOS")
    elseif(NOT CMAKE_HOST_SYSTEM_NAME STREQUAL "Linux" AND NOT CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")
        message(FATAL_ERROR "Unsupported platform: ${CMAKE_HOST_SYSTEM_NAME}")
    endif()
    return(PROPAGATE ${out_var})
endfunction()


function(pluginval_add_tests_if_enabled target)
    if(PLUGINVAL_TESTS_ENABLED)
        pluginval_add_tests(${target})
    endif()
endfunction()


function(pluginval_add_tests target)
    message(STATUS "Adding pluginval tests for ${target}")
    list(APPEND CMAKE_MESSAGE_INDENT ": ")

    get_target_property(active_targets ${target} JUCE_ACTIVE_PLUGIN_TARGETS)
    if(NOT active_targets)
        message(WARNING "No active plugin targets found for ${target}. Skipping...")
    endif()

    foreach(active_target ${active_targets})
        if(${active_target} MATCHES ".*(_VST|_VST3|_AU)$")
            get_target_property(plugin_path ${active_target} JUCE_PLUGIN_ARTEFACT_FILE)
            if(NOT plugin_path)
                message(WARNING "No plugin path found for ${active_target}. Skipping...")
                continue()
            endif()

            message(STATUS "${active_target}")
            add_test(NAME pluginval_${active_target}
                    COMMAND ${CMAKE_COMMAND} -DPLUGIN_PATH=${plugin_path} -P ${CMAKE_CURRENT_FUNCTION_LIST_FILE}
                    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
            )
        endif()
    endforeach()

    list(POP_BACK CMAKE_MESSAGE_INDENT)
endfunction()

########################################################################################################################

if(CMAKE_SCRIPT_MODE_FILE)
    if(NOT PLUGIN_PATH)
        message(FATAL_ERROR "Please provide the path to the plugin with '-DPLUGIN_PATH=<path>'")
    endif()

    if(NOT EXISTS ${PLUGIN_PATH})
        message(FATAL_ERROR "Plugin not found at path: ${PLUGIN_PATH}")
    endif()

    pluginval_validate(${PLUGIN_PATH})
endif()
