# CMake integration for clang-format linting checks
#
# Copyright (c) 2024 2-Shell (https://gitlab.com/2-shell/template-juce-full)
# License: MIT (https://gitlab.com/2-shell/template-juce-full/-/blob/main/LICENSE)
#
# This cmake module provides a way to enable clang-format based linting checks for a target.
# It will run clang-format on all sources of the target and its linked interface sources.
# The target must be a CMake target that has sources and/or interface sources.
#
# Defined cache variables:
# - CLANG_FORMAT_CHECK_ENABLED
#   - Enable clang-format based linting
#   - Default: OFF
# - CLANG_FORMAT_CHECK_PATH_BLACKLIST (Advanced)
#   - List of path prefixes to exclude from clang-format
#   - Default: ${CMAKE_BINARY_DIR}
# - CLANG_FORMAT_PROGRAM
#   - Path to the clang-format binary
#   - Default: (auto-detected)
#
# Public functions:
# - clang_format_check_activate(TARGET)
#   - Enable clang-format checks for the given target
# - clang_format_check_activate_if_configured(TARGET)
#   - Enable clang-format checks for the given target if the CLANG_FORMAT_CHECK_ENABLED option is set
#
# Added targets:
# - ${TARGET}-clang-format
#   - For each target, for which clang-format checks are enabled, a custom target is added that runs clang-format on all
#     sources of the target and its linked interface sources.
# - clang-format
#   - A custom target that depends on all ${TARGET}-clang-format targets.
#
# Private functions:
# - clang_format_get_target_linked_interface_sources(OUT_VAR TARGET)
#   - Get all linked interface sources of a target
# - clang_format_filter_linked_sources(OUT_VAR SOURCES)
#   - Filter out sources that are in the CLANG_FORMAT_PATH_BLACKLIST
#
# Dependencies:
# - CMake 3.12 or newer
# - LocalUtils.cmake
#
# Example usage:
# ```cmake
# add_executable(my_target main.cpp)
# clang_format_check_activate(my_target)
# ```
# or
# ```cmake
# add_executable(my_target main.cpp)
# clang_format_check_activate_if_configured(my_target)
# ```
#
# This will add a custom target `my_target-clang-format` that runs clang-format on `main.cpp`.
# The target `clang-format` will be added that depends on `my_target-clang-format`.
#
# Then run `cmake --build . --target clang-format` to run clang-format on all targets with clang-format checks enabled.
#
include_guard(GLOBAL)
include(${CMAKE_CURRENT_LIST_DIR}/LocalUtils.cmake)

env_option(CLANG_FORMAT_CHECK_ENABLED "Enable clang-format based linting" OFF)

set(CLANG_FORMAT_CHECK_PATH_BLACKLIST "${CMAKE_BINARY_DIR}" CACHE STRING
        "List of path prefixes to exclude from clang-format")
mark_as_advanced(CLANG_FORMAT_CHECK_PATH_BLACKLIST)


function(clang_format_check_activate_if_configured TARGET)
    if (CLANG_FORMAT_CHECK_ENABLED)
        find_program(CLANG_FORMAT_PROGRAM NAMES
                clang-format
                clang-format-20 clang-format-19 clang-format-18 clang-format-17 clang-format-16 clang-format-15
                clang-format-14 clang-format-13 clang-format-12 clang-format-11 clang-format-10 clang-format-9
                clang-format-8 clang-format-7 clang-format-6.0 clang-format-6
                DOC "Path to the clang-format binary"
        )
        clang_format_check_activate(${TARGET})
    endif()
endfunction()


function(clang_format_check_activate TARGET)
    if (CLANG_FORMAT_PROGRAM)
        message(STATUS "Enabling clang-format for ${TARGET}")
        get_target_property(target_sources ${TARGET} SOURCES)
        get_target_property(target_interface_sources ${TARGET} INTERFACE_SOURCES)

        clang_format_get_target_linked_interface_sources(target_linked_interface_sources ${TARGET})
        clang_format_filter_linked_sources(filtered_linked_interface_sources "${target_linked_interface_sources}")

        message(DEBUG "target_linked_interface_sources: ${target_linked_interface_sources}")
        message(DEBUG "filtered_linked_interface_sources: ${filtered_linked_interface_sources}")

        foreach (source ${target_sources} ${target_interface_sources} ${filtered_linked_interface_sources})
            if (source)
                cmake_path(ABSOLUTE_PATH source NORMALIZE)
                list(APPEND clang_format_sources ${source})
            endif()
        endforeach()

        add_custom_target(${TARGET}-clang-format
                COMMAND
                ${CLANG_FORMAT_PROGRAM} --output-replacements-xml --Werror -n --style=file ${clang_format_sources}
                WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                COMMENT "Running clang-format on ${TARGET}"
        )

        if (NOT TARGET clang-format)
            add_custom_target(clang-format)
        endif()

        add_dependencies(clang-format ${TARGET}-clang-format)
        message(DEBUG "Filtered linked interface sources: ${clang_format_sources}")
    else()
        message(WARNING "clang-format not found, skipping for ${TARGET}")
    endif()
endfunction()


function(clang_format_get_target_linked_interface_sources OUT_VAR TARGET)
    get_target_property(target_interface_sources ${TARGET} INTERFACE_SOURCES)
    foreach (source ${target_interface_sources})
        if (source)
            list(APPEND ${OUT_VAR} ${source})
        endif()
    endforeach()

    get_target_property(target_link_libraries ${TARGET} LINK_LIBRARIES)
    foreach (lib ${target_link_libraries})
        if (TARGET ${lib})
            clang_format_get_target_linked_interface_sources(sources ${lib})
            list(APPEND ${OUT_VAR} ${sources})
        endif()
    endforeach()

    return(PROPAGATE ${OUT_VAR})
endfunction()


function(clang_format_filter_linked_sources OUT_VAR SOURCES)
    foreach (source ${SOURCES})
        if (source)
            cmake_path(IS_PREFIX CMAKE_SOURCE_DIR "${source}" NORMALIZE is_in_source_dir)
            if(is_in_source_dir)
                foreach (path ${CLANG_FORMAT_PATH_BLACKLIST})
                    cmake_path(IS_PREFIX path "${source}" NORMALIZE is_in_blacklist)
                    if (NOT is_in_blacklist)
                        list(APPEND ${OUT_VAR} ${source})
                    endif()
                endforeach()
            endif()
        endif()
    endforeach()
    return(PROPAGATE ${OUT_VAR})
endfunction()
