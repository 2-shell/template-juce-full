# Project specific utilities and functions
include_guard(GLOBAL)

if(NOT CMAKE_SCRIPT_MODE_FILE)
    set(DEFAULT_BUILD_TYPE "Release")
    if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
        message(STATUS "Setting build type to '${DEFAULT_BUILD_TYPE}' as none was specified.")
        set(CMAKE_BUILD_TYPE "${DEFAULT_BUILD_TYPE}" CACHE STRING "Choose the type of build." FORCE)
        # Set the possible values of build type for cmake-gui
        set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
    endif()
endif()

macro(env_option NAME DESCRIPTION DEFAULT)
    set(${NAME}_DEFAULT ${DEFAULT})
    if(DEFINED ENV{${NAME}})
        set(${NAME}_DEFAULT $ENV{${NAME}})
    endif()
    option(${NAME} ${DESCRIPTION} ${${NAME}_DEFAULT})
endmacro()

macro(env_cache NAME DEFAULT TYPE DESCRIPTION)
    set(${NAME}_DEFAULT ${DEFAULT})
    if(DEFINED ENV{${NAME}})
        set(${NAME}_DEFAULT $ENV{${NAME}})
    endif()
    set(${NAME} ${${NAME}_DEFAULT} CACHE ${TYPE} ${DESCRIPTION})
endmacro()

function(add_subdirectory_if_enabled DIRECTORY TITLE DEFAULT)
    set(option_name "{{ cookiecutter.__cmake_var_prefix }}_BUILD_${TITLE}")
    string(MAKE_C_IDENTIFIER "${option_name}" option_name)
    string(TOUPPER "${option_name}" option_name)

    env_option("${option_name}" "Build ${TITLE}" ${DEFAULT})

    if(${option_name})
        MESSAGE(STATUS "[${TITLE}]: enabled")
        list(APPEND CMAKE_MESSAGE_INDENT ": ")
        add_subdirectory(${DIRECTORY})
        list(POP_BACK CMAKE_MESSAGE_INDENT)
    else()
        MESSAGE(STATUS "[${TITLE}]: SKIPPED")
    endif()
endfunction()

function(copy_if_exists TARGET PHASE FROM TO)
    # This is a shim to make CMake copy a whole directory / bundle or file if it exists, rather than just
    # the contents of a directory
    add_custom_command(
            TARGET ${TARGET} ${PHASE}
            BYPRODUCTS "${TO}"
            COMMAND "${CMAKE_COMMAND}"
            "-DSRC=${FROM}"
            "-DDEST=${TO}"
            "-P" "${CMAKE_CURRENT_FUNCTION_LIST_DIR}/copyIfExists.cmake"
            VERBATIM
    )
endfunction()

function(juce_target_collect_artefacts TARGET DEST_DIR)
    if(NOT TARGET ${TARGET})
        message(FATAL_ERROR "Target ${TARGET} does not exist!")
    endif()

    get_target_property(juce_product_name ${TARGET} JUCE_PRODUCT_NAME)
    get_target_property(juce_is_plugin ${TARGET} JUCE_IS_PLUGIN)

    if(NOT juce_product_name)
        message(FATAL_ERROR "Target ${TARGET} is not a JUCE target! Only targets created with"
                "juce_add_plugin(), juce_add_console_app() or juce_add_gui_app() are supported!")
    endif()

    if(juce_is_plugin)
        juce_plugin_target_collect_artefacts(${TARGET} ${DEST_DIR})
    else()
        juce_app_target_collect_artefacts(${TARGET} ${DEST_DIR})
    endif()
endfunction()

function(juce_plugin_target_collect_artefacts TARGET DEST_DIR)
    message(STATUS
            "Adding post-build commands for collecting artefacts of all"
            " active plugin formats of ${TARGET} to ${DEST_DIR} ...")

    get_target_property(PLUGIN_FORMATS ${TARGET} JUCE_FORMATS)
    if (NOT PLUGIN_FORMATS)
        message(STATUS "No plugin formats found!")
        message(FATAL_ERROR
                "Make sure target '${TARGET}' was added with juce_add_plugin()"
                " or use snsl_juce_app_target_collect_artefacts() instead!")
        return()
    endif ()

    list(APPEND CMAKE_MESSAGE_INDENT ": ")
    foreach (format ${PLUGIN_FORMATS})
        set(sub_target ${TARGET}_${format})
        if (TARGET ${sub_target})
            get_target_property(ARTEFACT_PATH ${sub_target} JUCE_PLUGIN_ARTEFACT_FILE)
            if (ARTEFACT_PATH)
                message(STATUS "${format}")
                juce_target_copy_artefact(${sub_target} POST_BUILD ${ARTEFACT_PATH} ${DEST_DIR}/${format})
            else ()
                message(WARNING "  => ${format} has no artefact file!")
            endif ()
        endif ()
    endforeach ()
    list(POP_BACK CMAKE_MESSAGE_INDENT)
endfunction()

function(juce_app_target_collect_artefacts TARGET DEST_DIR)
    message(STATUS "Adding post-build commands for collecting artefacts of ${TARGET} to ${DEST_DIR} ...")
    set(is_bundle "$<BOOL:$<TARGET_PROPERTY:${TARGET},BUNDLE>>")
    set(bundle_dir "$<${is_bundle}:$<TARGET_BUNDLE_DIR:${TARGET}>>")
    set(target_file "$<TARGET_FILE:${TARGET}>")
    set(src "$<IF:${is_bundle},${bundle_dir},${target_file}>")
    set(dest "${DEST_DIR}/${TARGET}")
    juce_target_copy_artefact("${TARGET}" POST_BUILD "${src}" "${dest}")
endfunction()

function(juce_target_copy_artefact TARGET PHASE SRC DEST)
    list(APPEND sources "${SRC}")
    if (APPLE)
        list(APPEND sources "${SRC}.dSYM")
    elseif (WIN32)
        list(APPEND sources "$<TARGET_PDB_FILE:${TARGET}>")
    endif ()
    copy_if_exists("${TARGET}" "${PHASE}" "${sources}" "${DEST}")
endfunction()

function(add_common_settings_target TARGET_NAME)
    # Add a virtual library target that communicates all common settings
    add_library(${TARGET_NAME} INTERFACE)

    target_compile_features(${TARGET_NAME} INTERFACE cxx_std_20)

    # Compiler specific settings
    # : MSVC
    set(if_msvc_cxx "$<COMPILE_LANG_AND_ID:CXX,MSVC>")
    # : : Enable RTTI
    target_compile_options(${TARGET_NAME} INTERFACE "$<${if_msvc_cxx}:/GR>")
    # : Clang-/GCC-like
    set(if_gcc_like_cxx "$<COMPILE_LANG_AND_ID:CXX,ARMClang,AppleClang,Clang,GNU,LCC>")
    # : : Mark JUCE headers as system headers
    target_compile_options(${TARGET_NAME} INTERFACE "$<${if_gcc_like_cxx}:--system-header-prefix=juce_>")
endfunction()

function(set_common_target_properties TARGET_NAME)
    set_target_properties(${TARGET_NAME} PROPERTIES
            # generate debug symbols in a .dsym file
            XCODE_ATTRIBUTE_GCC_GENERATE_DEBUGGING_SYMBOLS[variant=Release] YES
            XCODE_ATTRIBUTE_GCC_DEBUGGING_SYMBOLS[variant=Release] full
            XCODE_ATTRIBUTE_DEBUG_INFORMATION_FORMAT[variant=Release] dwarf-with-dsym
            XCODE_ATTRIBUTE_DEAD_CODE_STRIPPING[variant=Release] YES
            # strip symbols from final executable for release mode
            XCODE_ATTRIBUTE_DEPLOYMENT_POSTPROCESSING[variant=Release] YES
    )
endfunction()