include_guard(GLOBAL)
include(${CMAKE_CURRENT_LIST_DIR}/LocalUtils.cmake)

env_option(LLVM_COV_ENABLED "Enable LLVM coverage" OFF)

set(LLVM_COV_OUTPUT_DIR "${CMAKE_BINARY_DIR}/coverage" CACHE PATH "Output directory path for coverage report files")
mark_as_advanced(LLVM_COV_OUTPUT_DIR)

set(LLVM_COV_IGNORE_BUILD_DIR "${CMAKE_BINARY_DIR}")
cmake_path(NATIVE_PATH LLVM_COV_IGNORE_BUILD_DIR LLVM_COV_IGNORE_BUILD_DIR)
string(REPLACE "\\" "\\\\" LLVM_COV_IGNORE_BUILD_DIR "${LLVM_COV_IGNORE_BUILD_DIR}")

set(LLVM_COV_IGNORE_TEST_DIR "${CMAKE_SOURCE_DIR}/test")
cmake_path(NATIVE_PATH LLVM_COV_IGNORE_TEST_DIR LLVM_COV_IGNORE_TEST_DIR)
string(REPLACE "\\" "\\\\" LLVM_COV_IGNORE_TEST_DIR "${LLVM_COV_IGNORE_TEST_DIR}")

set(LLVM_COV_IGNORE_FILENAME_REGEX
        "(${LLVM_COV_IGNORE_BUILD_DIR}.*|${LLVM_COV_IGNORE_TEST_DIR}.*)"
        CACHE STRING "Regex to ignore files from coverage report"
)
mark_as_advanced(LLVM_COV_IGNORE_FILENAME_REGEX)


function(llvm_cov_activate_if_enabled target)
    if(LLVM_COV_ENABLED)
        llvm_cov_activate(${target})
    endif()
endfunction()


function(llvm_cov_activate target)
    message(STATUS "Enabling LLVM coverage for target ${target}")
    list(APPEND CMAKE_MESSAGE_INDENT ": ")

    llvm_cov_check_preconditions(${target})
    llvm_cov_add_flags(${target})

    llvm_cov_add_coverage_report(${target})

    list(POP_BACK CMAKE_MESSAGE_INDENT)
endfunction()


function(llvm_cov_check_preconditions target)
    if(NOT TARGET ${target})
        message(FATAL_ERROR "Target '${target}' does not exist")
    endif()

    get_property(generator_is_multi_config GLOBAL PROPERTY GENERATOR_IS_MULTI_CONFIG)
    if(NOT CMAKE_BUILD_TYPE STREQUAL "Debug" OR generator_is_multi_config)
        message(WARNING "Code coverage results with an optimised (non-Debug) build may be misleading")
    endif()

    get_TARGET_PROPERTY(type ${target} TYPE)
    if(NOT type STREQUAL "EXECUTABLE")
        message(FATAL_ERROR "Coverage is only supported for executables")
    endif()

    if(NOT "${CMAKE_CXX_COMPILER_ID}" MATCHES "(Apple)?[Cc]lang")
        message(FATAL_ERROR "LLVM Coverage is only supported with Clang")
    endif()

    include(CheckCXXCompilerFlag)
    check_cxx_compiler_flag("-fprofile-instr-generate" has_profile_instr_generate)
    check_cxx_compiler_flag("-fprofile-instr-generate -fcoverage-mapping" has_coverage_mapping)
    if(NOT has_profile_instr_generate OR NOT has_coverage_mapping)
        message(FATAL_ERROR "LLVM Coverage requires -fprofile-instr-generate and -fcoverage-mapping")
    endif()

    find_program(LLVM_PROFDATA_PROGRAM NAMES
            llvm-profdata llvm-profdata-20 llvm-profdata-19 llvm-profdata-18 llvm-profdata-17 llvm-profdata-16
            llvm-profdata-15 llvm-profdata-14 llvm-profdata-13 llvm-profdata-12 llvm-profdata-11 llvm-profdata-10
            llvm-profdata-9 llvm-profdata-8 llvm-profdata-7 llvm-profdata-6
            DOC "Path to the llvm-profdata binary"
            REQUIRED)
    find_program(LLVM_COV_PROGRAM NAMES
            llvm-cov llvm-cov-20 llvm-cov-19 llvm-cov-18 llvm-cov-17 llvm-cov-16 llvm-cov-15 llvm-cov-14 llvm-cov-13
            llvm-cov-12 llvm-cov-11 llvm-cov-10 llvm-cov-9 llvm-cov-8 llvm-cov-7 llvm-cov-6
            DOC "Path to the llvm-cov binary"
            REQUIRED)
    if(NOT LLVM_PROFDATA_PROGRAM OR NOT LLVM_COV_PROGRAM)
        message(FATAL_ERROR "llvm-profdata and llvm-cov are required to generate coverage reports")
        message(FATAL_ERROR "Please install llvm-tools and/or set LLVM_PROFDATA_PROGRAM and LLVM_COV_PROGRAM")
    endif()
endfunction()


function(llvm_cov_add_flags target)
    set(LLVM_COV_COMPILE_FLAGS "-fprofile-instr-generate" "-fcoverage-mapping")
    set(LLVM_COV_LINK_FLAGS "-fprofile-instr-generate" "-fcoverage-mapping")
    target_compile_options(${target} PUBLIC ${LLVM_COV_COMPILE_FLAGS})
    target_link_options(${target} PUBLIC ${LLVM_COV_LINK_FLAGS})
endfunction()


function(llvm_cov_add_coverage_report target)
    set(target_binary $<TARGET_FILE:${target}>)
    set(profraw_file ${target}_cov.profraw)
    set(profdata_file ${target}_cov.profdata)
    set(text_report ${target}_cov.txt)
    set(json_report ${target}_cov.json)
    set(html_report ${target}_cov.html)
    set(lcov_info ${target}_cov.lcov.info)
    set(llvm_cov_args -instr-profile=${profdata_file} -ignore-filename-regex="${LLVM_COV_IGNORE_FILENAME_REGEX}")

    file(MAKE_DIRECTORY ${LLVM_COV_OUTPUT_DIR})

    add_custom_command(OUTPUT ${profraw_file}
            COMMAND ${CMAKE_COMMAND} -E env LLVM_PROFILE_FILE=${profraw_file} ${target_binary}
            DEPENDS ${target}
            WORKING_DIRECTORY ${LLVM_COV_OUTPUT_DIR}
            COMMENT "Running ${target} with llvm coverage"
    )

    add_custom_command(OUTPUT ${profdata_file}
            COMMAND ${LLVM_PROFDATA_PROGRAM} merge -sparse ${profraw_file} -o ${profdata_file}
            DEPENDS ${profraw_file}
            WORKING_DIRECTORY ${LLVM_COV_OUTPUT_DIR}
            COMMENT "Generating llvm coverage profile for ${target}"
    )

    add_custom_command(OUTPUT ${text_report}
            COMMAND ${LLVM_COV_PROGRAM} report ${target_binary} ${llvm_cov_args} > ${text_report}
            DEPENDS ${profdata_file}
            WORKING_DIRECTORY ${LLVM_COV_OUTPUT_DIR}
            COMMENT "Generating llvm coverage plaintext report for ${target}"
    )

    add_custom_command(OUTPUT ${json_report}
            COMMAND ${LLVM_COV_PROGRAM} export ${target_binary} ${llvm_cov_args} -format=text > ${json_report}
            DEPENDS ${profdata_file}
            WORKING_DIRECTORY ${LLVM_COV_OUTPUT_DIR}
            COMMENT "Generating llvm coverage JSON report for ${target}"
    )

    add_custom_command(OUTPUT ${html_report}
            COMMAND ${LLVM_COV_PROGRAM} show ${target_binary} ${llvm_cov_args} -format=html -output-dir=${html_report}
            DEPENDS ${profdata_file}
            WORKING_DIRECTORY ${LLVM_COV_OUTPUT_DIR}
            COMMENT "Generating llvm coverage HTML report for ${target}"
    )

    add_custom_command(OUTPUT ${lcov_info}
            COMMAND ${LLVM_COV_PROGRAM} export ${target_binary} ${llvm_cov_args} -format=lcov > ${lcov_info}
            DEPENDS ${profdata_file}
            WORKING_DIRECTORY ${LLVM_COV_OUTPUT_DIR}
            COMMENT "Generating llvm coverage LCOV report for ${target}"
    )

    add_custom_target(${target}-coverage
            DEPENDS ${profdata_file} ${text_report} ${json_report} ${html_report} ${lcov_info}
            COMMAND ${LLVM_COV_PROGRAM} report ${target_binary} ${llvm_cov_args}
            WORKING_DIRECTORY ${LLVM_COV_OUTPUT_DIR}
            COMMENT "Generating llvm coverage reports for ${target}"
    )

    if(NOT TARGET coverage)
        add_custom_target(coverage)
    endif()

    add_dependencies(coverage ${target}-coverage)

endfunction()
