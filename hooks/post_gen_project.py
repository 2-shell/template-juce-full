import os
import shutil

REMOVE_PATHS = [
    '{% if not cookiecutter.target_cli %}src/cli{% endif %}',
    '{% if not cookiecutter.target_gui %}src/gui{% endif %}',
    '{% if not cookiecutter.target_plugin %}src/plugin{% endif %}',
    '{% if not cookiecutter.target_cli_add_data_target and not cookiecutter.target_gui_add_data_target and not cookiecutter.target_plugin_add_data_target %}resources/juce_logo.png{% endif %}',

]

for path in REMOVE_PATHS:
    path = path.strip()
    if path and os.path.exists(path):
        os.unlink(path) if os.path.isfile(path) else shutil.rmtree(path, ignore_errors=True)
